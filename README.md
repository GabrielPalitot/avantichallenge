<h1> SOBRE O DESAFIO</h1>

O intuito do desafio é criar a infraestrutura com terraform na AWS, criar um deploy com o GitLab, na EC2 na AWS. Por fim, testando o nosso conhecimento adquirido até aqui pelo bootcamp Avanti DevOps.

<h1>Processo</h1>

Você pode acessar os arquivos de código utilizados do Terraform por aqui: https://gitlab.com/GabrielPalitot/avantichallenge

O passo a passo simplificado é o seguinte:

- Criar uma instância EC2 por meio do uso do Terraform.

- Criar um deploy com o Gitlab usando pipeline.


<h2>Criando uma instância EC2 por meio do uso do Terraform</h2>

Para isso, vamos usar os arquivos já feitos em aulas anteriores.

Vamos usar o comando `terraform plan` que  cria um plano de execução, que permite visualizar as alterações que o Terraform planeja fazer em sua infraestrutura atual.

- Executando o `terraform plan`

<img src="./imgs/terraformplanimg.png">

Estamos criando 3 security groups que são recursos utilizados para definir regras de controle de acesso de entrada (ingress) e saída (egress) para instâncias em redes virtuais. Então estamos definindo para permitir SSH e HTTP, além de se conectar na internet. Após dar uma rápida olhada no `terraform plan`, podemos partir para o `terraform apply` comando que executa as ações propostas no `terraform plan`

- Executando o `terraform apply`

<img src="./imgs/terraformapply.png">

- Vendo a Instância criada na AWS:

<img src="./imgs/awssuccess.png">

<h2> Criando um deploy com o Gitlab usando pipeline.</h2>

Como a instância foi criada e os comandos em `script.sh` foram executados, então o gitlab-runner já foi instalado. 

O repositório já foi criado, então vamos registrar primeiramente o runner que irá rodar no servidor aws. Para isso, depois de criada a instância, entramos no seu console e rodamos os seguintes comandos de acordo com o gitlab para registrar o runner, por segurança o token de registro foi ocultado.

`sudo gitlab-runner register --url https://gitlab.com/ --registration-token tokendeRegistro-`

- Registro do Runner

<img src="./imgs/registerrunner.png">

- Sucesso no Gitlab

<img src="./imgs/runnergitlab.png">

O arquivo de pipeline está em `.gitlab-ci.yml`.

Para que o gitlab-runner consiga modificar a pasta do Apache, precisamos mudar o "dono" da pasta, então fazemos o seguinte comando:

```bash
sudo chown gitlab-runner /var/www/html/ -R
```

<img src="./imgs/gitlab-runnerdono.png">

Com isso, o próximo passo é dar um push para o repositório criado no gitlab que dessa forma, ele vai identificar que há um arquivo .yml e executará o pipeline que é para levar a pasta `app/` para `/var/www/html/`.

- Sucesso Pipeline

<img src="./imgs/pipelinesuccess.png">

Vamos ver o site, basta acessarmos no google com o ip da máquina, ou com `http://ip-máquina-virtual`

<img src="./imgs/successdeploy.png">




